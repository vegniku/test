from github import Github
from pprint import pprint
from py2neo import Graph, Node, Relationship,NodeMatcher

#连接neo4j
test_graph = Graph("http://localhost:7474",auth=("neo4j","test1"))
#清空所有结点
test_graph.delete_all()

#结点类
class node:
    type_=''
    #初始化并创建结点
    def __init__(self,number,title,assignees,state,locked,labels,milestone,user,comments,body,created_time,closed_time,updated_time,id,url,pull_request):
        test_node_1 = Node("issue",
            number=number,
            title=title,
            assignees=self.list2str(assignees),
            state=state,
            locked=locked,
            labels=self.list2str(labels),
            milestone=str(milestone),
            comments=comments,
            user=str(user),
            body=body,      
            created_time=str(created_time),
            closed_time=str(closed_time),
            updated_time=str(updated_time),
            id=id,
            url=url,
            pull_request_url=self.ispr(pull_request),
            node_type=self.type_)
        test_graph.create(test_node_1)
    #将列表转化成字符串
    def list2str(self,lists):
        node_list=''
        for list_ in lists:
            node_list+=str(list_)
        return node_list
    #判断是issue还是pull request
    def ispr(self,pr):
        if not pr:
            url="none"
            self.type_="issue"
        else:
            url=pr.html_url
            self.type_="pull request"
        return url
#查找函数，查找含有特定label的issue
def find(str):
    str0="MATCH (n:issue)WHERE n.labels=~'.*"
    str0+=str
    str0+=".*' RETURN n"
    data = test_graph.run(str0)
    print(data)
    return data
# Github username
username = "vegniku"
#使用令牌登陆接触速度限制
token="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"
# connect github
g = Github(token)
#要爬取的页面url
urls={"tensorflow/federated","FederatedAI/FATE","PaddlePaddle/PaddleFL","OpenMined/PySyft"}
for url in urls:
    repo=g.get_repo(url)
    issues=repo.get_issues(state="closed")
    for issue in issues:
        mynode=node(issue.number,issue.title,issue.assignees,issue.state,issue.locked,issue.labels,
            issue.milestone,issue.user,issue.comments,issue.body,issue.created_at,issue.closed_at,issue.updated_at,issue.id,issue.url,issue.pull_request)

