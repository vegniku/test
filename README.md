# test
本项目用于存放漏洞收集与测试的代码，关联[federatedlearningbugs](https://gitlab.com/justinwm/taxonomyoffederatedlearningbugs/-/issues/1)

其中get_issues.py为爬虫程序，在程序中设置要爬取的url和自己的登陆口令即可开始爬取数据

从get_issues()中提取的结构与有意义的字段如下表：
| variable | meaning |
| ------ | ------ |
|id| issue的id值|
|number|issue的编号，和pull request共享|
|assignee|issue 的assignee的登录名列表| 
|title|issue的标题|
|body |issue详细内容|
|create_at| issue的创建时间|
|closed_at|issue的关闭时间|
|updated_at|issue的更新时间|
|labels |issue的标签列表|
|milestone |issue的milestone的标题,设计版本演变历史|
|state| issue是开放或关闭|
|user |issue的创建者 |
|locked|issue是否锁定|
|comments|issue的评论|
|url|记录该issue的url|
|pull_request | 如果该结构是PR，则记录url|

- 使用neo4j数据库存储，每个节点表示一个issue，设置的属性同上表类似。
  同时新增加一个属性node_type，通过pull_request确定该结点类型(issue或PR)并记录。

- 本次数据的收集时间2021-07-23-11:29:46,共收集了9839个项目。

- 在脚本中通过模糊查找，实现了查找所有含特定标签的函数。
